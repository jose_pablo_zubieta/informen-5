# %%

# Diseño Programa que permite calcular más de 1500 decimales de pi.
# -⁻- coding: UTF-8 -*-

print("Decimales de pi")
print("  ")


def arccot(x, unity):
    sum = xpower = unity // x
    n = 3
    sign = -1
    while 1:
        xpower = xpower // (x * x)
        term = xpower // n
        if not term:
            break
        sum += sign * term
        sign = -sign
        n += 2
    return sum


def pi(digits):
    unity = 10 ** (digits + 10)
    pi = 4 * (4 * arccot(5, unity) - arccot(239, unity))
    return pi // 10 ** 10


# En este caso se calculan los 1500 primeros decimales de π. aL Modificar valor para calcular los requeridos

print(pi(1500))


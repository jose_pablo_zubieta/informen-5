#clase de institucion
class universidad:
  def __init__(self,nombre_universidad,ubicacion,tiempo_operacion):
    self.Nuniversidad=nombre_universidad
    self.ubicacion=ubicacion
    self.Toperacion=tiempo_operacion

  def get_nombre_universidad(self):
    return self.Nuniversidad

  def get_ubicacion(self):
    return self.ubicacion

  def get_tiempo_operacion(self):
    return self.Toperacion

  def set_nombre_universidad(self,nuevo_nombre):
    self.Nuniversidad=nuevo_nombre

  def set_ubicacion(self,nueva_ubicacion):
    self.ubicacion=nueva_ubicacion

  def set_tiempo_operacion(self,nuevo_tiempo_operacion):
    self.Toperacion=nuevo_tiempo_operacion

  def universidad(self):
    print(f"la institucion estudiantil se llama {self.Nuniversidad}")

  def donde_esta(self):
    print(f"la {self.Nuniversidad} esta ubicada en {self.ubicacion}")

  def tiempo_funcionando(self):
    print(f"la {self.Nuniversidad} lleva funcionando {self.Toperacion} años")
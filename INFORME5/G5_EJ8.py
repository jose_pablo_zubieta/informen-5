# %%

# identificar las variables Y Diferenciarlas segun tipo
def main():
    varA = ('10')
    varB = ('10')
    print('varA: ', varA, ':', type(varA))
    print('varB: ', varB, ':', type(varB))
    print('La varA es:')
    if (type(varA) == type(varB)):
        if (type(varA) == str):
            print("string involucrado")
        elif (varA == varB):
            print("Ambas variables son iguales")
        elif (type(varA) == int):
            if (varA > varB):
                print("mas grande")
            elif (varA < varB):
                print("más pequeño")
            else:
                print("error1")
        else:
            print("error2")
    else:
        print("la varA es distinto tipo que B")


main()

class rector(universidad):

  def __init__(self,nombre_universidad,ubicacion,tiempo_operacion,nombre_completo_del_rector,ubicacion_trabajo,profesion,tiempo_trabajo_rector):
    super().__init__(nombre_universidad,ubicacion,tiempo_operacion)
    self.nombre_rector=nombre_completo_del_rector
    self.ubi_trabajo=ubicacion_trabajo
    self.tiempo_trabajo=tiempo_trabajo_rector
    self.profesion=profesion

  def get_nombre_completo_del_rector(self):
    return self.nombre_rector

  def get_ubicacion_trabajo(self):
    return self.ubi_trabajo

  def get_profesion(self):
    return self.profesion

  def get_tiempo_trabajo_rector(self):
    return self.tiempo_trabajo

  def set_nombre_completo_del_rector(self,nuevo_nombre_rector):
    self.nombre_rector=nuevo_nombre_rector

  def set_ubicacion_trabajo(self,nueva_ubicacion):
    self.ubi_trabajo=nueva_ubicacion

  def set_profesion(self,nueva_profesion):
    self.profesion=nueva_profesion

  def set_tiempo_trabajo(self,nuevo_tiempo_trabajo):
    self.tiempo_trabajo=nuevo_tiempo_trabajo

  def quien_es(self):
    print(f"El Sr/Sra. {self.nombre_rector} es un(a) {self.profesion}")

  def donde_trabaja(self):
    if self.ubicacion==self.ubi_trabajo:
      print(f"El Sr/Sra. {self.nombre_rector} es el rector de la {self.Nuniversidad}")
    else:
      print(f"El Sr/Sra. {self.nombre_rector} no es el rector de la {self.Nuniversidad} porque trabaja en {self.ubi_trabajo}" )
  def cuanto_lleva_ejerciendo(self):
    print(f"El Sr/Sra. {self.nombre_rector} lleva ejerciendo su cargo {self.tiempo_trabajo} años hasta la fecha")